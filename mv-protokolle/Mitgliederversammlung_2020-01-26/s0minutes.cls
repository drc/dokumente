%%
%% This is file `s0minutes.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% s0minutes.dtx  (with options: `class')
%% 
%% Copyright (C) 2017 Roland Hieber <rohieb+latex@rohieb.name>
%% 
%% This file may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in:
%% 
%%   http://www.latex-project.org/lppl.txt
%% 
%% and version 1.3 or later is part of all distributions of
%% LaTeX version 2005/12/01 or later.
%% 
\def\thisclass{s0minutes}
\def\thisfile{\thisclass.dtx}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{\thisclass}
[2018/07/17 v1.1 Stratum 0 Corporate Identity -- Meeting Minutes]
\DeclareOption*{\InputIfFileExists{\CurrentOption.s0c}{}{%
\PassOptionsToClass{\CurrentOption}{s0artcl}}}
\ProcessOptions \relax
\LoadClass{s0artcl}
\RequirePackage{xifthen}  % \isempty test
\RequirePackage{framed}   % resolution rendering etc.
\RequirePackage{dingbat}  % \leftthumbsup etc.
\RequirePackage[svgnames,table]{xcolor}
\RequirePackage{tabularx}
\RequirePackage[ngerman,orig]{isodate}
\let\theoldsection\thesection
\renewcommand*{\thesection}{TOP \theoldsection}
\setcounter{section}{-1} % Real hackers(TM) start counting at 0
\settowidth{\cftsecnumwidth}{TOP 10 }
\settowidth{\cftsubsecnumwidth}{TOP 1.10 }
\ohead{\textsc{\printdate{\sn@date}}}
\newcommand*{\meetingminutes}[7]{%
\def\sn@typeofmeeting{#1}%
\def\sn@date{#2}%
\def\sn@startingtime{#3}%
\def\sn@place{#4}%
\def\sn@attendants{#5}%
\def\sn@absentees{#6}%
\def\sn@minutetaker{#7}%
\title{\sn@typeofmeeting{} \sn@date}%
\date{}
\subject{Stratum~0~e.\,V.}
}
\def\boardmeeting{Vorstandssitzung}
\def\generalassembly{Mitgliederversammlung}
\newcommand*{\sn@meetinginfo}{%
\begin{tabularx}{.8\textwidth}{rX}
Ort: & \sn@place \\
Zeit: & \printdate{\sn@date}, \sn@startingtime \\
Anwesend: & \sn@attendants \\
\ifx\sn@absentees\empty
\empty
\else
Abwesend: & \sn@absentees \\
\fi
Protokoll: & \sn@minutetaker \\
\end{tabularx}
}
\def\@author{\sn@meetinginfo} %% This is just a dirty hack to get it into \maketitle
\newcommand{\meetingpreamble}{\sn@meetinginfo}

\newcommand*{\meetingend}[1]{%
\vskip 33pt minus 33pt
\begin{samepage}
Es gibt keine weiteren Tagesordnungspunkte. Die Versammlung wird geschlossen.
\begin{description}
\item[Ende:] #1
\end{description}
\end{samepage}
\clearpage
\sn@signatures
}

\newcommand*{\sn@signatures}{%
\section*{Unterschriften}
\begin{tabular}{@{}lp{.6\textwidth}}
Protokollführer:       & \hrulefill\rule{0pt}{1.5cm}\phantom{c} \\
Vorstandsvorsitzender: & \hrulefill\rule{0pt}{1.5cm}\phantom{c} \\
Stellv. Vorsitzender:  & \hrulefill\rule{0pt}{1.5cm}\phantom{c} \\
Schatzmeister:         & \hrulefill\rule{0pt}{1.5cm}\phantom{c} \\
Beisitzer:             & \hrulefill\rule{0pt}{1.5cm}\phantom{c} \\
Beisitzer:             & \hrulefill\rule{0pt}{1.5cm}\phantom{c} \\
Beisitzer:             & \hrulefill\rule{0pt}{1.5cm}\phantom{c} \\
\end{tabular}
}

\newcommand*{\sn@resolution}[4]{%
\bgroup\raggedright% when breaking, prevent stretching the line to be justified
\textbf{#1: #3}\ifthenelse{\equal{#4}{}}{}{ (#4)}%
\allowbreak% prefer breaking here
\hspace*{\fill}% when breaking, stretch this line to be right-aligned
\hspace*{.5em}\mbox{#2}% keep #2 together and have a bit of slack
\egroup%
}
\colorlet{shadecolor}{gray!40}%
\newenvironment{resolution}[4]{%
\begin{shaded}
\sn@resolution{#1}{#2}{#3}{#4}%
\par
}{% try to even out the upper and lower padding of the shaded box...
\vspace{-.2\baselineskip}%
\end{shaded}
\vspace{-\parskip}%
\vspace{-\FrameSep}%
\vspace{\ifdim\FrameSep<\parskip\parskip\else\FrameSep\fi}%
}

\newcommand{\adopted}{Angenommen \leftthumbsup}
\newcommand{\rejected}{Abgelehnt \rightthumbsdown}
\newcommand{\vote}[4]{#1 (#2/#3/#4)}
\newcommand{\consensus}[1]{#1 (Konsens)}

\newenvironment{electionblock}{%
\begin{description}
}{%
\end{description}
}

\newcommand{\elected}[3]{\item #1:  #2, #3}

\newcommand{\meetingbreak}[1]{
\begin{center}
\emph{(Pause #1)}
\end{center}
}

\newcommand{\question}[2][]{\textbf{Frage\ifthenelse{\isempty{#1}}{}{ (#1)}:\hspace{1ex}}#2 \\}
\newcommand{\answer}[2][]{\textbf{Antwort\ifthenelse{\isempty{#1}}{}{ (#1)}:\hspace{1ex}}#2}

\endinput
%%
%% End of file `s0minutes.cls'.
